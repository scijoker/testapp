package ablack13.testapp.util.sort

import ablack13.testapp.extension.safeLet
import ablack13.testapp.service.dto.Hotel

class HotelComparator(private val sort: SortType) : Comparator<Hotel> {

    override fun compare(hotel1: Hotel?, hotel2: Hotel?): Int =
        safeLet(hotel1, hotel2) { h1, h2 ->
            compare(sort, h1, h2)
        } ?: 0

    private fun compare(sort: SortType, hotel1: Hotel, hotel2: Hotel): Int =
        when (sort) {
            SortType.DEFAULT -> compareTo(hotel1.order, hotel2.order)
            SortType.DISTANCE_19 -> compareTo(hotel2.distance, hotel1.distance)
            SortType.DISTANCE_91 -> compareTo(hotel1.distance, hotel2.distance)
            SortType.AVAILABLE_SUITES_19 -> compareTo(hotel2.suites, hotel1.suites)
            SortType.AVAILABLE_SUITES_91 -> compareTo(hotel1.suites, hotel2.suites)
        }

    private fun <K : Comparable<K>> compareTo(value0: K, value1: K): Int =
        compare(sort.weigth, value0.compareTo(value1))

    private fun compare(weight: Int, result: Int): Int =
        when {
            result > 0 -> weight
            result < 0 -> -weight
            else -> 0
        }
}