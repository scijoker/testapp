package ablack13.testapp.util.sort

import ablack13.testapp.service.dto.Hotel

object HotelSortUtil {
    var currentSort = SortType.DEFAULT
        private set

    fun sortedWith(src: List<Hotel>?, sortType: SortType = SortType.DEFAULT): List<Hotel> {
        currentSort = sortType
        return src?.sortedWith(HotelComparator(currentSort)) ?: emptyList()
    }
}