package ablack13.testapp.util.sort

enum class SortType(val weigth: Int) {
    DEFAULT(1 shl 0),
    DISTANCE_19(1 shl 0),
    DISTANCE_91(1 shl 1),
    AVAILABLE_SUITES_19(1 shl 2),
    AVAILABLE_SUITES_91(1 shl 2),
}