package ablack13.testapp.util

import android.content.Context
import androidx.fragment.app.Fragment
import java.util.*

object BindingLayoutResUtil {
    private val defLayoutPrefix = listOf("activity", "fragment", "view", "item")

    fun getLayoutResId(ctx: Context, prefix: List<String> = defLayoutPrefix): Int =
        ctx.getIndentifier(formLayoutName(ctx.javaClass, prefix))

    fun getLayoutResId(f: Fragment, prefix: List<String> = defLayoutPrefix): Int =
        f.requireActivity().getIndentifier(formLayoutName(f.javaClass, prefix))

    private fun Context.getIndentifier(layoutId: String): Int =
        resources.getIdentifier(layoutId, "layout", packageName)

    private fun formLayoutName(clazz: Class<*>, prefix: List<String> = defLayoutPrefix): String =
        clazz.simpleName
            .replace(Regex("[A-Z]")) { "_${it.value.toLowerCase(Locale.getDefault())}" }
            .split("_")
            .filter { it.isNotEmpty() }
            .toMutableList()
            .run {
                val startWord = first { prefix.contains(it) }.apply { remove(this) }
                "${startWord}_${joinToString(separator = "_")}"//form layoutId name
            }
}