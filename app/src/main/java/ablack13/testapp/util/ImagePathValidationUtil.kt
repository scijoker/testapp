package ablack13.testapp.util

import ablack13.testapp.service.Endpoint

object ImagePathValidationUtil {
    fun formPath(imageId: String?): String? =
        if (!imageId.isNullOrEmpty())
            "${Endpoint.baseURL}instadevteam/tests/raw/63a9ecea18ca79c275a2eeafd95bc37f857cf2ec/1.2/$imageId"
        else
            null
}