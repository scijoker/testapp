package ablack13.testapp.provider

import ablack13.testapp.service.dto.Hotel

interface HotelServiceProvider {
    suspend fun getHotels(): List<Hotel>
}