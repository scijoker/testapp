package ablack13.testapp.provider.impl

import ablack13.testapp.provider.HotelServiceProvider
import ablack13.testapp.service.RestClientProvider
import ablack13.testapp.service.api.WebServiceApi
import ablack13.testapp.service.dto.Hotel

class HotelsServiceProviderImpl(restClientProvider: RestClientProvider) : HotelServiceProvider {
    private val serviceApi = restClientProvider.instance.create(WebServiceApi::class.java)

    override suspend fun getHotels(): List<Hotel> =
        with(serviceApi) {
            getHotelIds()
                .mapIndexed { index, hotel -> getHotel(hotel.id).apply { order = index } }
                .toList()
        }
}