package ablack13.testapp.app

import ablack13.testapp.di.DiProvider
import ablack13.testapp.di.appModule
import android.app.Application

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        DiProvider.startEngine(
            context = applicationContext,
            modules = listOf(
                appModule(isInDebugMode = false)
            )
        )
    }
}