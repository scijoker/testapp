package ablack13.testapp.common

import ablack13.testapp.di.DiComponent
import androidx.lifecycle.ViewModel

abstract class ViewModel : ViewModel(), DiComponent