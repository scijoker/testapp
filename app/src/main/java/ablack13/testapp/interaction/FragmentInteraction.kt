package ablack13.testapp.interaction

import ablack13.testapp.ui.common.CommonFragment

interface FragmentInteraction {
    val panelId: Int
    fun replaceInPanel(fragment: CommonFragment<*>, needAddToBackStack: Boolean = true)
}