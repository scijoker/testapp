package ablack13.testapp.extension

import ablack13.testapp.ui.common.CommonFragment
import android.os.Bundle
import kotlin.reflect.full.createInstance


fun <T : CommonFragment<*>> T.args(op: Bundle.() -> Unit): T =
    apply {
        arguments = Bundle().apply(op)
    }

inline fun <reified T : CommonFragment<*>> createFragment(
    noinline args: Bundle.() -> Unit={}
): T =
    T::class.createInstance().args(args)
