package ablack13.testapp.extension

import ablack13.testapp.common.ViewModel
import androidx.fragment.app.Fragment
import org.koin.androidx.viewmodel.ext.android.getSharedViewModel
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier

inline fun <reified T : ViewModel> Fragment.sharedViewModel(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null
) =
    lazy(LazyThreadSafetyMode.NONE) { getSharedViewModel<T>(qualifier, parameters) }