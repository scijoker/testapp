package ablack13.testapp.extension

import ablack13.testapp.ui.common.CommonActivity
import ablack13.testapp.ui.common.CommonFragment
import ablack13.testapp.util.BindingLayoutResUtil

val CommonActivity<*>.bindingLayoutResId: Int
    get() = BindingLayoutResUtil.getLayoutResId(this)

val CommonFragment<*>.bindingLayoutResId: Int
    get() = BindingLayoutResUtil.getLayoutResId(this)

fun CommonActivity<*>.bindinLayoutResId(prefix: List<String>) =
    BindingLayoutResUtil.getLayoutResId(this, prefix)

fun CommonFragment<*>.bindinLayoutResId(prefix: List<String>) =
    BindingLayoutResUtil.getLayoutResId(this, prefix)