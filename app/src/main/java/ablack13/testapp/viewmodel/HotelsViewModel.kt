package ablack13.testapp.viewmodel

import ablack13.testapp.common.ViewModel
import ablack13.testapp.provider.HotelServiceProvider
import ablack13.testapp.service.dto.Hotel
import ablack13.testapp.util.sort.HotelSortUtil
import ablack13.testapp.util.sort.SortType
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class HotelsViewModel(private val serviceProvider: HotelServiceProvider) : ViewModel() {
    val loadingState = MutableLiveData(false)
    val hotels = MutableLiveData<List<Hotel>>()

    fun loadData() {
        viewModelScope.launch {
            loadingState.value = true
            hotels.value = HotelSortUtil.sortedWith(serviceProvider.getHotels())
            loadingState.value = false
        }
    }

    fun onSortChanged(sortType: SortType) {
        viewModelScope.launch {
            hotels.value = HotelSortUtil.sortedWith(hotels.value, sortType)
        }
    }

    fun getCurrentSort() =
        HotelSortUtil.currentSort

    fun getHotelById(hotelId: Int?): Hotel? =
        hotels.value?.firstOrNull { it.id == hotelId }
}