package ablack13.testapp.ui.extension

import ablack13.testapp.interaction.FragmentInteraction
import ablack13.testapp.interaction.Toolbarinteraction
import ablack13.testapp.ui.common.CommonFragment
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar

fun CommonFragment<*>.toolbar(op: Toolbar.() -> Unit) {
    activity?.let { if (it is Toolbarinteraction) op(it.toolbarView) }
}

fun CommonFragment<*>.replaceFragment(fragment: CommonFragment<*>) {
    activity?.let { if (it is FragmentInteraction) it.replaceInPanel(fragment) }
}

var Toolbar.titleId: Int
    get() = throw NotImplementedError("Not support")
    set(value) {
        setTitle(value)
    }

fun Toolbar.clearMenu() =
    menu.clear()

fun Toolbar.onItemClick(op: (item: MenuItem) -> Unit) {
    setOnMenuItemClickListener {
        op(it)
        true
    }
}

fun Toolbar.setMenu(menu: Int, onClick: (item: MenuItem) -> Unit) {
    clearMenu()
    inflateMenu(menu)
    onItemClick(onClick)
}

fun MenuItem.clearIco(): MenuItem =
    apply { icon = null }
