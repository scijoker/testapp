package ablack13.testapp.ui.fragment

import ablack13.testapp.R
import ablack13.testapp.databinding.FragmentHotelDetailInfoBinding
import ablack13.testapp.extension.ImageViewLoader
import ablack13.testapp.extension.createFragment
import ablack13.testapp.extension.sharedViewModel
import ablack13.testapp.service.dto.Hotel
import ablack13.testapp.ui.common.CommonFragment
import ablack13.testapp.ui.extension.clearMenu
import ablack13.testapp.ui.extension.toolbar
import ablack13.testapp.viewmodel.HotelsViewModel
import android.os.Bundle
import android.view.View

class HotelDetailInfoFragment : CommonFragment<FragmentHotelDetailInfoBinding>() {
    private val viewModel by sharedViewModel<HotelsViewModel>()

    companion object {
        const val ARGUMENT_HOTEL_ID = "arg_hotel_id"

        fun newInstance(hotelId: Int): HotelDetailInfoFragment =
            createFragment { putInt(ARGUMENT_HOTEL_ID, hotelId) }
    }

    private fun initToolbar() {
        toolbar {
            clearMenu()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUI()
        loadHoteInfoIntoViews()
    }

    private fun initUI() {
        initToolbar()
    }

    private fun loadHoteInfoIntoViews() {
        viewModel.getHotelById(arguments?.getInt(ARGUMENT_HOTEL_ID))?.let {
            setHotelNameInToolbar(it)
            setHotelDetailInfoIntoCard(it)
        }
    }

    private fun setHotelNameInToolbar(hotel: Hotel) {
        toolbar {
            title = hotel.name
        }
    }

    private fun setHotelDetailInfoIntoCard(hotel: Hotel) {
        with(hotel) {
            with(binding) {
                tvAddress.text = getString(R.string.text_address, address)
                tvRating.text = getString(R.string.text_rating, stars.toString())
                tvSuites.text = getString(R.string.text_available_suites, suites.toString())
                ImageViewLoader.with(requireActivity()).load(imgUrl).into(ivImage)
            }
        }
    }
}