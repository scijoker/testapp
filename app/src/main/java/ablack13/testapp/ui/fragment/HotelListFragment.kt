package ablack13.testapp.ui.fragment

import ablack13.testapp.R
import ablack13.testapp.adapter.HotelsListAdapter
import ablack13.testapp.databinding.FragmentHotelListBinding
import ablack13.testapp.extension.sharedViewModel
import ablack13.testapp.service.dto.Hotel
import ablack13.testapp.ui.common.CommonFragment
import ablack13.testapp.ui.extension.*
import ablack13.testapp.util.sort.SortType
import ablack13.testapp.viewmodel.HotelsViewModel
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager

class HotelListFragment : CommonFragment<FragmentHotelListBinding>() {
    private val viewModel by sharedViewModel<HotelsViewModel>()
    private val hotelsAdapter = HotelsListAdapter().apply { setCallback { openHotelDetails(it) } }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUI()
        loadListData()
    }

    private fun initUI() {
        initToolbar()
        initRecyclerView()
        initProgressBar()
    }

    private fun initToolbar() {
        toolbar {
            titleId = R.string.text_hotels
            setMenu(
                menu = R.menu.menu_hotel_list,
                onClick = {
                    when (it.itemId) {
                        R.id.menu_action_sort_default -> onSortChanged(SortType.DEFAULT)
                        R.id.menu_action_sort_distance_19 -> onSortChanged(SortType.DISTANCE_19)
                        R.id.menu_action_sort_distance_91 -> onSortChanged(SortType.DISTANCE_91)
                        R.id.menu_action_sort_available_suites_19 -> onSortChanged(SortType.AVAILABLE_SUITES_19)
                        R.id.menu_action_sort_available_suites_91 -> onSortChanged(SortType.AVAILABLE_SUITES_91)
                    }
                    postDelayed({ initToolbar() }, 10)
                })
            selectCurrentHotelSortTypeInToolbar(this)
        }
    }

    private fun selectCurrentHotelSortTypeInToolbar(toolbar: Toolbar) {
        val menu = toolbar.menu

        val default = menu.findItem(R.id.menu_action_sort_default).clearIco()
        val distance19 = menu.findItem(R.id.menu_action_sort_distance_19).clearIco()
        val distance91 = menu.findItem(R.id.menu_action_sort_distance_91).clearIco()
        val suite19 = menu.findItem(R.id.menu_action_sort_available_suites_19).clearIco()
        val suite91 = menu.findItem(R.id.menu_action_sort_available_suites_91).clearIco()

        val doneIco = R.drawable.ic_done

        when (viewModel.getCurrentSort()) {
            SortType.DEFAULT -> default.setIcon(doneIco)
            SortType.DISTANCE_19 -> distance19.setIcon(doneIco)
            SortType.DISTANCE_91 -> distance91.setIcon(doneIco)
            SortType.AVAILABLE_SUITES_19 -> suite19.setIcon(doneIco)
            SortType.AVAILABLE_SUITES_91 -> suite91.setIcon(doneIco)
        }
    }

    private fun initRecyclerView() {
        with(binding.recyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = hotelsAdapter
        }
    }

    private fun initProgressBar() {
        with(viewModel) {
            loadingState.observe(viewLifecycleOwner) {
                with(binding.progressbar) {
                    visibility =
                        if (it) View.VISIBLE
                        else View.GONE
                }
            }

            //startup setup
            with(binding.progressbar) {
                visibility =
                    if (loadingState.value == true) View.VISIBLE
                    else View.GONE
            }
        }
    }


    private fun loadListData() {
        with(viewModel) {
            hotels.observe(viewLifecycleOwner, {
                hotelsAdapter.updateData(it)
            })

            if (hotels.value.isNullOrEmpty()) {
                loadData()
            }
        }
    }

    private fun openHotelDetails(hotel: Hotel) =
        replaceFragment(HotelDetailInfoFragment.newInstance(hotelId = hotel.id))

    private fun onSortChanged(sortType: SortType) =
        viewModel.onSortChanged(sortType)
}