package ablack13.testapp.ui.common

import ablack13.testapp.extension.bindingLayoutResId
import ablack13.testapp.interaction.FragmentInteraction
import ablack13.testapp.interaction.Toolbarinteraction
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.viewbinding.ViewBinding

abstract class CommonActivity<T : ViewBinding> : AppCompatActivity(), FragmentInteraction {
    private var _binding: T? = null
    protected val binding: T
        get() = _binding!!

    private val panelTag = "panel_tag"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding = DataBindingUtil.setContentView(this, bindingLayoutResId)
    }

    override fun replaceInPanel(fragment: CommonFragment<*>, needAddToBackStack: Boolean) {
        supportFragmentManager.beginTransaction()
            .apply {
                replace(panelId, fragment, panelTag)
                if (needAddToBackStack)
                    addToBackStack("${panelTag}_${fragment.javaClass::class.java.name}")
                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            }
            .commit()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}