package ablack13.testapp.ui.activity

import ablack13.testapp.R
import ablack13.testapp.databinding.ActivityMainBinding
import ablack13.testapp.extension.createFragment
import ablack13.testapp.interaction.Toolbarinteraction
import ablack13.testapp.ui.common.CommonActivity
import ablack13.testapp.ui.fragment.HotelListFragment
import android.os.Bundle
import androidx.appcompat.widget.Toolbar

class MainActivity : CommonActivity<ActivityMainBinding>(), Toolbarinteraction {

    override val toolbarView: Toolbar
        get() = binding.toolbarView

    override val panelId: Int = R.id.flContainer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null)
            replaceInPanel(createFragment<HotelListFragment>(), false)
    }
}