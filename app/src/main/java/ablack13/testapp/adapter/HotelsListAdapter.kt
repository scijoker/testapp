package ablack13.testapp.adapter

import ablack13.testapp.R
import ablack13.testapp.service.dto.Hotel
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class HotelsListAdapter : RecyclerView.Adapter<Holder>() {
    private var items: List<Hotel> = emptyList()
    private var callback: ((hotel: Hotel) -> Unit)? = null

    fun updateData(data: List<Hotel>) {
        val oldSize = items.size
        items = data
        if (oldSize > 0)
            notifyItemRangeChanged(0, oldSize)
        else
            notifyDataSetChanged()
    }

    fun setCallback(op: (hotel: Hotel) -> Unit) {
        callback = op
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder =
        Holder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_hotels_list, parent, false)
        )

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val hotel = items[position]
        with(holder) {
            tvName.text = hotel.name
            llContainer.setOnClickListener { callback?.invoke(hotel) }
        }
    }

    override fun setHasStableIds(hasStableIds: Boolean) {
        super.setHasStableIds(true)
    }

    override fun getItemCount(): Int =
        items.size
}


class Holder(item: View) : RecyclerView.ViewHolder(item) {
    val llContainer: LinearLayout = item.findViewById(R.id.llContainer)
    val tvName: TextView = item.findViewById(R.id.tvName)
}