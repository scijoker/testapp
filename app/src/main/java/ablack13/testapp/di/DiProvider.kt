package ablack13.testapp.di

import android.content.Context
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.KoinContextHandler
import org.koin.core.context.startKoin
import org.koin.core.module.Module

object DiProvider {
    fun startEngine(context: Context? = null, modules: List<Module>) {
        startKoin {
            if (context != null)
                androidContext(context)
            modules(modules)
        }
    }

    fun stopEngine() =
        KoinContextHandler.stop()
}