package ablack13.testapp.di

import ablack13.testapp.provider.HotelServiceProvider
import ablack13.testapp.provider.impl.HotelsServiceProviderImpl
import ablack13.testapp.service.Endpoint
import ablack13.testapp.service.RestClientProvider
import ablack13.testapp.viewmodel.HotelsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.bind
import org.koin.dsl.module

fun appModule(isInDebugMode: Boolean) =
    module {
        single { RestClientProvider(isInDebugMode, Endpoint.baseURL) }
        single { HotelsServiceProviderImpl(get()) } bind HotelServiceProvider::class
        viewModel { HotelsViewModel(get()) }
    }