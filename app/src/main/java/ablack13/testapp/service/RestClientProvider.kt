package ablack13.testapp.service

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RestClientProvider(private val isDebug: Boolean, private val baseUrl: String) {
    val instance by lazy { provideClient() }


    private fun provideClient(): Retrofit {
        val gsonFactory = GsonConverterFactory.create(GsonBuilder().apply { setLenient() }.create())
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(gsonFactory)
            .client(provideHttpClient(isDebug))
            .build()
    }

    private fun provideHttpClient(isDebug: Boolean) =
        OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .pingInterval(10, TimeUnit.SECONDS)
            .addInterceptor(provideInterceptor(isDebug))
            .build()

    private fun provideInterceptor(isDebug: Boolean): Interceptor {
        val logging = HttpLoggingInterceptor()
        if (isDebug) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }
        return logging
    }
}