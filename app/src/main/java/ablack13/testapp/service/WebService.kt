package ablack13.testapp.service

import ablack13.testapp.service.dto.Hotel

interface WebService {
    suspend fun getHotelList(): List<Hotel>
}
