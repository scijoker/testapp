package ablack13.testapp.service.api

import ablack13.testapp.service.dto.Hotel
import ablack13.testapp.service.dto.HotelId
import retrofit2.http.GET
import retrofit2.http.Path

interface WebServiceApi {
    @GET("instadevteam/tests/raw/63a9ecea18ca79c275a2eeafd95bc37f857cf2ec/1.2/0777.json")
    suspend fun getHotelIds(): List<HotelId>

    @GET("instadevteam/tests/raw/63a9ecea18ca79c275a2eeafd95bc37f857cf2ec/1.2/{id}.json")
    suspend fun getHotel(@Path("id") hotelId: Int): Hotel
}