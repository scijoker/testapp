package ablack13.testapp.service.dto

import ablack13.testapp.util.ImagePathValidationUtil
import com.google.gson.annotations.SerializedName

data class Hotel(
    val id: Int,
    val name: String,
    val address: String,
    val stars: Double,
    val distance: Double,
    private val image: String,
    @SerializedName("suites_availability")
    private val suites_availability: String,
    val lat: Double,
    val log: Double
) {
    var order: Int = 0

    var imgUrl: String? = null
        get() {
            if (field == null)
                field = ImagePathValidationUtil.formPath(image)
            return field
        }
        private set

    var suites: Int = 0
        get() {
            if (field == 0) {
                field = suites_availability.split(":").size
            }
            return field
        }
        private set
}