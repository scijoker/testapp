package ablack13.testapp.test.common

import ablack13.testapp.di.DiComponent
import ablack13.testapp.di.DiProvider
import ablack13.testapp.di.appModule
import org.junit.After
import org.junit.Before
import org.koin.core.module.Module

open class CommonTest(private val isInDebugMode: Boolean = false) : DiComponent {
    @Before
    fun setUp() {
        DiProvider.startEngine(modules = listOf(getDiModule()))
    }

    open fun getDiModule(): Module =
        appModule(isInDebugMode)

    @After
    fun tearDown() {
        DiProvider.stopEngine()
    }
}