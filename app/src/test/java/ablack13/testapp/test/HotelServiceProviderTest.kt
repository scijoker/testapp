package ablack13.testapp.test

import ablack13.testapp.provider.HotelServiceProvider
import ablack13.testapp.test.common.CommonTest
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.koin.core.inject

class HotelServiceProviderTest : CommonTest(isInDebugMode = true) {
    private val hotelServiceProvider by inject<HotelServiceProvider>()

    @Test
    fun getHotels() = runBlocking {
        Assert.assertNotNull(hotelServiceProvider)
        val hotels = hotelServiceProvider.getHotels()
        hotels.forEach {
            it.suites
            it.imgUrl
        }
        Assert.assertNotNull(hotels)
    }
}