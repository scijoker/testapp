package ablack13.testapp.test

import org.junit.Test

class ComponentNameParserTest {
    private val src = "SomeSpecialCodeActivity"

    @Test
    fun start() {
        val name = getLayoutName(src)
        println("activity:[$src] layoutRes:[$name]")
    }

    private fun getLayoutName(widgetName: String, prefix: String? = null): String {
        val result =
            widgetName.replace(Regex("[A-Z]")) { matchResult -> "_" + matchResult.value.toLowerCase() }
                .split("_").filter { it.isNotEmpty() }.toMutableList()
        val prefix1 = prefix
            ?: result.firstOrNull { it == "activity" || it == "fragment" || it == "view" || it == "item" }
        result.remove(prefix1)
        return "${prefix1}_${result.joinToString(separator = "_")}"
    }
}